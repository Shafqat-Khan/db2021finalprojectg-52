﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBG35FProject
{
    public partial class Staff : Form
    {
        public Staff()
        {
            InitializeComponent();
        }

        private void Staff_Load(object sender, EventArgs e)
        {
            DropDown.BackColor = Color.FromArgb(100, 0, 0, 0);
            main_Panel.BackColor = Color.FromArgb(100, 0, 0, 0);
        }      

        private void exit_Click_1(object sender, EventArgs e)
        {
            DialogResult dia;
            dia = MessageBox.Show("Do You want to Exit", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dia == DialogResult.Yes)
            {
                this.Hide();
                DashBoard db = new DashBoard();
                db.Show();
            }
            else
            {
                this.Show();
            }
        }

        private void attendence_Click(object sender, EventArgs e)
        {
            this.main_Panel.Controls.Clear();
            Attendence atd = new Attendence() { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
            this.main_Panel.Controls.Add(atd);
            atd.Show();
        }

        private void bill_Click(object sender, EventArgs e)
        {
            this.main_Panel.Controls.Clear();
            Generate_Bill gb = new Generate_Bill() { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
            this.main_Panel.Controls.Add(gb);
            gb.Show();
        }

        private void record_Click(object sender, EventArgs e)
        {
            this.main_Panel.Controls.Clear();
            record rc = new record() { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
            this.main_Panel.Controls.Add(rc);
            rc.Show();
        }

        private void mess_Click(object sender, EventArgs e)
        {
            this.main_Panel.Controls.Clear();
            Food fd = new Food() { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
            this.main_Panel.Controls.Add(fd);
            fd.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void guna2CircleButton1_Click(object sender, EventArgs e)
        {
            this.main_Panel.Controls.Clear();
            atttenda ad = new atttenda() { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
            this.main_Panel.Controls.Add(ad);
            ad.Show();
        }
    }
}